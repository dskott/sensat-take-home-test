### Usage instructions
- Clone the repo locally
- `$ cd sensat-take-home-test/`
- `$ npm run buildInstallCLIapp`
- `$ read_sensor_data [--box=<box_id>] [--from=<from_date>] [--to=<to_date>] [--aggregate=true]`

### Requirements
- CLI app:
    - reads particular arguments from CL
    - writes output to STDOUT as JSON
- Talks to DB on AWS
    - return rows with particular values for columns, including range of values for a col
    - do "range scan" of DB to obtain aggregated values
    - read DB config from yaml file
- Expose HTTP interface

