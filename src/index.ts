#!/usr/bin/env node

import { exit } from "process";
import express from "express";
import * as bodyParser from "body-parser";
import { DBreader } from "./classes/DBreader";
import { DBwrapperCLI } from "./classes/DBwrapperCLI";
import { readingRouter } from "./routes/readingRouter";

const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const path = require('path');
const program = require('commander');


// Show starting screen
clear();
console.log(
  chalk.red(
    figlet.textSync('sensor-data', { horizontalLayout: 'full' })
  )
);

// Define CLI options
program
  .version('1.0.0')
  .description("CLI for reading sensor data from DB")
  .option('--box <box_id>', 'Define box_id for sensors')
  .option('--from <from_datetime>', 'Set starting datetime')
  .option('--to <to_datetime>', 'Set final datetime')
  .option('--aggregate <true>', 'Return aggregated data for filtered sensors and boxes')
  .option('-h, --help', 'Show help screen')
  .parse(process.argv);

const options = program.opts();

// Show the help screen
if (options.help) {
  program.outputHelp();
  process.exit();
}

// Output identified inputs
// if (!options.box) console.log("Box:", options.box);
// if (options.from) console.log("From:", options.from);
// if (options.to) console.log("To:", options.to);
// if (options.aggregate) {
//   if (options.aggregate === "true"){
//     console.log("aggregate true");
//   } else {
//     console.log("expected aggregate input true or false, %s given", options.aggregate);
//   }
// }

// CLI logic
const dbReader = new DBreader();
const dbWrapper = new DBwrapperCLI(dbReader);

if ((options.aggregate) && (options.aggregate === "true")){
  dbWrapper.getAggregateRecords(options.box, options.from, options.to);
} else {
  dbWrapper.getRecords(options.box, options.from, options.to);
}

// server stuff
const app = express();
app.use(bodyParser.json());
app.use("/readings", readingRouter);

const serverPort: number = 8000;
app.listen(serverPort, () => {
  console.log("Node server started running");
});