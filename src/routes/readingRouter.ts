import express, {Request, Response} from "express";
import { DBreader } from "../classes/DBreader"
import { Record } from "../types/OutputTypes";

const readingRouter = express.Router();
const dbReaderLocal = new DBreader();

readingRouter.get("/:boxId/:fromDate/:toDate", async (req: Request, res: Response) => {
    console.log("GET readings/:boxId/:fromDate/:toDate")
    const box_id: String = String(req.params.boxId);
    const from_date: String = String(req.params.fromDate);
    const to_date: String = String(req.params.toDate);

    dbReaderLocal.getRecords(box_id, from_date, to_date, (err: Error, records: Record[]) => {
        if (err) {
          return res.status(500).json({"message": err.message});
        }
        res.status(200).json({"data": records});
    });
});

export {readingRouter};
