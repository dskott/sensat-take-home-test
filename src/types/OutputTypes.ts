export interface RecordMetadata {
    box_id: string,
    sensor_id: string,
    name: string,
    unit: string
};

export interface Record extends RecordMetadata {
    reading: number,
    reading_ts: Date
};

export interface AggregateRecord extends RecordMetadata {
    min_reading: number,
    max_reading: number,
    avg_reading: number
};