import mysql from "mysql2";
import { OkPacket, RowDataPacket } from "mysql2";
import { Record, AggregateRecord } from "../types/OutputTypes.js";
const yaml = require('js-yaml');
const fs   = require('fs');


export class DBreader {
    private db: mysql.Connection;

    constructor(){
        // Read db configuration from conf.yaml
        // may throw exception which we are not handling since without 
        // the db settings, the whole program cannot function because it 
        // can't retrieve data
        const doc = yaml.load(fs.readFileSync('./conf.yaml', 'utf8')); 
        const dbSettings = {
            username: doc.username,
            password: doc.password,
            host: doc.host,
            port: doc.port,
            dbName: doc.dbName
        }

        this.db = mysql.createConnection({
            host: dbSettings.host,
            user: dbSettings.username,
            password: dbSettings.password,
            database: dbSettings.dbName,
            port: dbSettings.port
        });
    }

    getRecords(box_id: String, fromDate: String, toDate: String, callback: Function){
        // parse inputs and convert undefined to null
        box_id = this.valueOrNull(box_id);
        fromDate = this.valueOrNull(fromDate);
        toDate = this.valueOrNull(toDate);

        // get optional fitlers for SQL query
        const filterQueryPortion = this.getSqlFilterExpression(box_id, fromDate, toDate);

        const queryString = `
        SELECT 
        r.box_id, 
        r.sensor_id, 
        s.name, 
        s.unit, 
        r.reading, 
        r.reading_ts 
        FROM readings AS r 
        INNER JOIN sensors AS s ON s.id=r.sensor_id 
        WHERE 1 = 1` 
        + filterQueryPortion.queryFilter; // append the filtering part of the query
  
        this.db.query(
        queryString,
        filterQueryPortion.filterParams,
        (err, result) => {
            if (err) {
                callback(err, null);
                console.log("Error reading from db");
                return;
            };

            const records: Record[] = [];
            const rows = <RowDataPacket[]> result;
        
            rows.forEach(row => {
              const record: Record =  {
                box_id: row.box_id,
                sensor_id: row.sensor_id,
                name: row.name,
                unit: row.unit,
                reading: row.reading,
                reading_ts: row.reading_ts
              }
              records.push(record);
            });
            callback(null, records);   
        });
    };

    getAggregateRecords(box_id: String, fromDate: String, toDate: String, callback: Function){
        // parse inputs and convert undefined to null
        box_id = this.valueOrNull(box_id);
        fromDate = this.valueOrNull(fromDate);
        toDate = this.valueOrNull(toDate);

        // get optional fitlers for SQL query
        const filterQueryPortion = this.getSqlFilterExpression(box_id, fromDate, toDate);

        let queryString = `
        SELECT 
        r.box_id, 
        r.sensor_id, 
        s.name, 
        s.unit, 
        MIN(r.reading) AS min, 
        MAX(r.reading) AS max, 
        AVG(r.reading) AS avg 
        FROM readings AS r 
        INNER JOIN sensors AS s ON r.sensor_id=s.id
        WHERE 1=1 `
        + filterQueryPortion.queryFilter

        // the space is concatenated between the box_id and sensor_id, 
        // because that character does not exist in any of the two strings.
        // Hence, we can safely group by the resulting string and have the 
        // same result as if we grouped by the combination of the two columns.
        + `
        GROUP BY CONCAT(r.box_id, " ", r.sensor_id)`; 

        this.db.query(
        queryString,
        filterQueryPortion.filterParams,
        (err, result) => {
            if (err) {
                callback(err, null);
                console.log("Error reading from db");
                return;
            };

            const aggRecords: AggregateRecord[] = [];
            const rows = <RowDataPacket[]> result;
        
            rows.forEach(row => {
                const aggRecord: AggregateRecord =  {
                    box_id: row.box_id,
                    sensor_id: row.sensor_id,
                    name: row.name,
                    unit: row.unit,
                    min_reading: row.min,
                    max_reading: row.max,
                    avg_reading: row.avg
                }
                aggRecords.push(aggRecord);
            });
            callback(null, aggRecords);   
        });
    };

    valueOrNull(val:String){
        return val || null;
    };

    getSqlFilterExpression(box_id: String, fromDate: String, toDate: String){
        let queryFilter = ``;
        const filterParams: String[] = [];

        if (box_id !== null){
            queryFilter += `
            AND r.box_id=?`;
            filterParams.push(box_id);
        }
        if (fromDate !== null){
            queryFilter += `
            AND reading_ts >= STR_TO_DATE(?, '%Y-%m-%dT%H:%i:%s')`;
            filterParams.push(fromDate);
        }
        if (toDate !== null){
            queryFilter += `
            AND reading_ts <= STR_TO_DATE(?, '%Y-%m-%dT%H:%i:%s')`;
            filterParams.push(toDate);
        }

        return {queryFilter: queryFilter, filterParams: filterParams};
    };
}