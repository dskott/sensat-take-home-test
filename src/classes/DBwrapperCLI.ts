import { DBreader } from "./DBreader";
import { AggregateRecord, Record } from "../types/OutputTypes";

export class DBwrapperCLI {
    constructor(public dbReader: DBreader){};

    getRecords(box_id: String, fromDate: String, toDate: String){
        this.dbReader.getRecords(box_id, fromDate, toDate, (err: Error, records: Record[]) => {
            if (err){
              console.log(err);
            } else {
              console.log({data: records});
            }
        });
    }

    getAggregateRecords(box_id: String, fromDate: String, toDate: String){
        this.dbReader.getAggregateRecords(box_id, fromDate, toDate, (err: Error, aggRecords: AggregateRecord[]) => {
            if (err){
              console.log(err);
            } else {
              console.log({data: aggRecords});
            }
        });
    }
}